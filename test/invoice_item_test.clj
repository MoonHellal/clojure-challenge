(ns invoice-item-test
  (:require [clojure.test :refer :all]
            [invoice-item :refer :all]))

(deftest test-subtotal-negative-discount
  (testing "subtotal returns the correct value with negative discount"
    (let [item {:invoice-item/precise-quantity 2
                :invoice-item/precise-price 10
                :invoice-item/discount-rate -10}]
      (is (= 22.0 (subtotal item))))))

(deftest test-subtotal
  (testing "subtotal returns the correct value"
    (let [item {:invoice-item/precise-quantity 2
                :invoice-item/precise-price 10
                :invoice-item/discount-rate 10}]
      (is (= 18.0 (subtotal item))))
    (let [item {:invoice-item/precise-quantity 3
                :invoice-item/precise-price 20
                :invoice-item/discount-rate 0}]
      (is (= 60.0 (subtotal item))))))

(deftest test-subtotal-large-discount
  (testing "subtotal returns the correct value with large discount"
    (let [item {:invoice-item/precise-quantity 2
                :invoice-item/precise-price 10
                :invoice-item/discount-rate 90}]
      (is (= 2 (subtotal item))))))

(deftest test-subtotal-over-100-discount
  (testing "subtotal returns the correct value with discount over 100"
    (let [item {:invoice-item/precise-quantity 2
                :invoice-item/precise-price 10
                :invoice-item/discount-rate 110}]
      (is (= -2 (subtotal item))))))



(deftest test-subtotal-zero-quantity
  (testing "subtotal returns 0 with zero quantity"
    (let [item {:invoice-item/precise-quantity 0
                :invoice-item/precise-price 10
                :invoice-item/discount-rate 10}]
      (is (= 0.0 (subtotal item))))))

(deftest test-subtotal-zero-price
  (testing "subtotal returns 0 with zero price"
    (let [item {:invoice-item/precise-quantity 2
                :invoice-item/precise-price 0
                :invoice-item/discount-rate 10}]
      (is (= 0.0 (subtotal item))))))



