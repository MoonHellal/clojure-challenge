(ns challenge-1-test
  (:require [clojure.test :refer :all]
            [challenge-1 :refer :all]))

(deftest test-filter-invoice
  (deftest test-filter-invoice-empty
    (let [invoice {:invoice/items []}]
      (is (empty? (filter-invoice invoice)))))
  (deftest test-filter-invoice-no-items
    (let [invoice {}]
      (is (empty? (filter-invoice invoice)))))
  (deftest test-filter-invoice-no-taxes-or-retentions
    (let [invoice {:invoice/items [{:name "item1"} {:name "item2"}]}]
      (is (empty? (filter-invoice invoice)))))
  )