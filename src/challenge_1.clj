(ns challenge-1)
(require '[clojure.java.io :as io])

(def invoice (clojure.edn/read-string (slurp (io/resource "invoice.edn"))))

(defn filter-invoice [invoice]
"Takes a map an return the items that match with the filter "
  (->> invoice
       :invoice/items
       (filter (fn [{:keys [taxable/taxes retentionable/retentions]}]
                 (or (and (= 19 (:tax/rate (first taxes))) (not= 1 (:retentions/rate (first retentions))))
                     (and (not= 19 (:tax/rate (first taxes))) ( = 1 (:retentions/rate (first retentions)))))))))

(def filtered-invoice (filter-invoice invoice))

(println filtered-invoice)


