(ns invoice-spec
  (:require
    [clojure.java.io :as io]
    [clojure.spec.alpha :as s]
    [clojure.data.json :as json])
  )

(s/def :customer/name string?)
(s/def :customer/email string?)
(s/def :invoice/customer (s/keys :req [:customer/name
                                       :customer/email]))

(s/def :tax/rate double?)
(s/def :tax/category #{:iva})
(s/def ::tax (s/keys :req [:tax/category
                           :tax/rate]))
(s/def :invoice-item/taxes (s/coll-of ::tax :kind vector? :min-count 1))

(s/def :invoice-item/price double?)
(s/def :invoice-item/quantity double?)
(s/def :invoice-item/sku string?)

(s/def ::invoice-item
  (s/keys :req [:invoice-item/price
                :invoice-item/quantity
                :invoice-item/sku
                :invoice-item/taxes]))

(s/def :invoice/issue-date inst?)
(s/def :invoice/items (s/coll-of ::invoice-item :kind vector? :min-count 1))

(s/def ::invoice
  (s/keys :req [:invoice/issue-date
                :invoice/customer
                :invoice/items]))
(defn json-to-map [file-name]
  (let [json (json/read (clojure.java.io/reader (io/resource file-name)) :key-fn keyword)
        invoice-data (:invoice json)
        items (for [i (:items invoice-data)]
                {:invoice-item/price    (:price i)
                 :invoice-item/quantity (:quantity i)
                 :invoice-item/sku      (:sku i)
                 :invoice-item/taxes    (vec (for [j (:taxes i)]
                                          {:tax/category :iva
                                           :tax/rate (Double/parseDouble (str (:tax_rate j)))}))
                 })]
    {
     :invoice/issue-date (.parse (java.text.SimpleDateFormat. "dd/MM/yyyy") (get-in invoice-data [:issue_date]))
     :invoice/customer   {:customer/name  (get-in invoice-data [:customer :company_name])
                          :customer/email (get-in invoice-data [:customer :email])}
     :invoice/items       (vec items)}))

(def invoice (json-to-map "invoice.json"))
(println (s/valid? ::invoice  invoice))
